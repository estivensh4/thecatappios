## Arquitectura

El proyecto utiliza el patrón de arquitectura MVVM.

[App Android](https://gitlab.com/estivensh4/thecatapp)

## Capturas

| <img src="screenshots/dashboard.png" width=200/> | <img src="screenshots/detail.png" width=200/> |
|:------------------------------------------------:|:---------------------------------------------:|
