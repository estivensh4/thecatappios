//
//  TheCatAppIosApp.swift
//  TheCatAppIos
//
//  Created by Estiven on 28/07/23.
//

import SwiftUI

@main
struct TheCatAppIosApp: App {
    @StateObject var networkMonitor = NetworkMonitor()

    var body: some Scene {
        WindowGroup {
            ContentView()
                .environmentObject(networkMonitor)
        }
    }
}
