//
//  BreedsInDTO.swift
//  TheCatAppIos
//
//  Created by Estiven on 28/07/23.
//

import Foundation

struct BreedsInDTO: Codable, Identifiable {
    
    var id               : String = ""
    var name             : String = ""
    var weight           : Weight = Weight()
    var temperament      : String = ""
    var origin           : String = ""
    var lifeSpan         : String = ""
    var wikipediaUrl     : String = ""
    var referenceImageId : String = ""
    var image            : String = ""
    var description            : String = ""

    
    enum CodingKeys: String, CodingKey {
        case id               = "id"
        case name             = "name"
        case weight           = "weight"
        case temperament      = "temperament"
        case origin           = "origin"
        case lifeSpan         = "life_span"
        case wikipediaUrl     = "wikipedia_url"
        case referenceImageId = "reference_image_id"
        case image = "image"
        case description = "description"
    }
    
    init(from decoder: Decoder) throws {
        let values = try decoder.container(keyedBy: CodingKeys.self)

        weight           = try values.decodeIfPresent(Weight.self , forKey: .weight           ) ?? Weight()
        id               = try values.decodeIfPresent(String.self , forKey: .id               ) ?? ""
        name             = try values.decodeIfPresent(String.self , forKey: .name             ) ?? ""
        temperament      = try values.decodeIfPresent(String.self , forKey: .temperament      ) ?? ""
        origin           = try values.decodeIfPresent(String.self , forKey: .origin           ) ?? ""
        lifeSpan         = try values.decodeIfPresent(String.self , forKey: .lifeSpan         ) ?? ""
        wikipediaUrl     = try values.decodeIfPresent(String.self , forKey: .wikipediaUrl     ) ?? ""
        referenceImageId = try values.decodeIfPresent(String.self , forKey: .referenceImageId ) ?? ""
        image = try values.decodeIfPresent(String.self , forKey: .image ) ?? ""
        description = try values.decodeIfPresent(String.self , forKey: .description ) ?? ""
      }

      init() {

      }
}


struct Weight: Codable {
    
    var imperial : String = ""
    var metric   : String = ""
    
    enum CodingKeys: String, CodingKey {
        
        case imperial = "imperial"
        case metric   = "metric"
        
    }
}
