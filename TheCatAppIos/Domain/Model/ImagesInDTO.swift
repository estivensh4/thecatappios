//
//  ImagesInDTO.swift
//  TheCatAppIos
//
//  Created by Estiven on 28/07/23.
//

import Foundation

struct ImagesInDTO: Decodable {
    var url: String
}
