//
//  NetworkManager.swift
//  TheCatAppIos
//
//  Created by Estiven on 28/07/23.
//

import Foundation

enum HTTPMethod: String {
    case get = "GET"
    case post = "POST"
    case put = "PUT"
    case delete = "DELETE"
}

class NetworkManager {
    static let shared = NetworkManager()
    
    private func createRequest(
        with url: URL,
        method: HTTPMethod,
        parameters: [String: Any]?
    ) -> URLRequest {
        var request = URLRequest(url: url)
        request.httpMethod = method.rawValue
        
        if method == .post || method == .put {
            if let parameters = parameters {
                do {
                    request.httpBody = try JSONSerialization.data(withJSONObject: parameters)
                    request.setValue("application/json", forHTTPHeaderField:  "Content-Type")
                } catch {
                    fatalError("Error encoding parameters \(error)")
                }
            }
        }
        
        return request
    }
    
    private func handleResponse<T: Decodable>(
        data: Data?,
        response: URLResponse?,
        error: Error?,
        completion: @escaping (Result<T, NetworkError>) -> Void
    ) {
        if let error = error {
            completion(.failure(.serverError(error)))
            return
        }
        
        guard let data = data else {
            completion(.failure(.noData))
            return
        }
        
        do {
            let decodeData = try JSONDecoder().decode(T.self, from: data)
            completion(.success(decodeData))
        } catch {
            completion(.failure(.decodingError(error)))
        }
    }
    
    func getData<T:Decodable>(
        endpoint: String,
        parameters: [String: Any]? = nil,
        completion: @escaping (Result<T, NetworkError>) -> Void
    ) {
        guard let url = URL(string: "https://api.thecatapi.com/" + endpoint) else {
            completion(.failure(.invalidURL))
            return
        }
        
        let request = createRequest(with: url, method: .get, parameters: parameters)
        
        let task = URLSession.shared.dataTask(with: request){data,response,error in
            self.handleResponse(data: data, response: response, error: error, completion: completion)
        }
        task.resume()
    }
    
    
    func postData<T: Decodable, U: Encodable>(
        endpoint: String,
        data: U,
        completion: @escaping (Result<T, NetworkError>) -> Void
    ) {
        guard let url = URL(string: "https://api.thecatapi.com/" + endpoint) else {
            completion(.failure(.invalidURL))
            return
        }
        
        let request = createRequest(with: url, method: .post, parameters: data as? [String : Any])
        
        let task = URLSession.shared.dataTask(with: request){ data, response, error in
            self.handleResponse(data: data, response: response, error: error, completion: completion)
        }
        task.resume()
    }
    
    func putData<T: Decodable, U: Encodable>(endpoint: String, data: U, completion: @escaping (Result<T, NetworkError>) -> Void) {
           guard let url = URL(string: "https://api.thecatapi.com/" + endpoint) else {
               completion(.failure(.invalidURL))
               return
           }
           
           let request = createRequest(with: url, method: .put, parameters: data as? [String : Any])
           
           let task = URLSession.shared.dataTask(with: request) { data, response, error in
               self.handleResponse(data: data, response: response, error: error, completion: completion)
           }
           task.resume()
       }
       
       func deleteData<T: Decodable>(endpoint: String, completion: @escaping (Result<T, NetworkError>) -> Void) {
           guard let url = URL(string: "https://api.thecatapi.com/" + endpoint) else {
               completion(.failure(.invalidURL))
               return
           }
           
           let request = createRequest(with: url, method: .delete, parameters: nil)
           
           let task = URLSession.shared.dataTask(with: request) { data, response, error in
               self.handleResponse(data: data, response: response, error: error, completion: completion)
           }
           task.resume()
       }
    
}
