//
//  NetworkError.swift
//  TheCatAppIos
//
//  Created by Estiven on 28/07/23.
//

import Foundation

enum NetworkError: Error {
    case invalidURL
        case serverError(Error)
        case noData
        case decodingError(Error)
        case encodingError(Error)
}
