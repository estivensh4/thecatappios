//
//  ItemBreed.swift
//  TheCatAppIos
//
//  Created by Estiven on 28/07/23.
//

import Foundation
import SwiftUI

struct ItemBreed : View {
    
    var breed: BreedsInDTO
    
    var body: some View {
        
        VStack {
            AsyncImage(url: URL(string: breed.image)) { image in
                image.resizable()
            } placeholder: {
                ProgressView()
                    .progressViewStyle(CircularProgressViewStyle(tint: .white))
                    .scaleEffect(1.5, anchor: .center)
            }
            .frame(maxWidth: .infinity, minHeight: 140,maxHeight: 140)
            VStack {
                Text(breed.name)
                    .lineLimit(1)
                    .foregroundColor(Color.primaryVariant)
                    .bold()
                    .frame(maxWidth: .infinity, alignment: .leading)
                Spacer().frame(height: 2)
                Text(breed.description)
                    .lineLimit(2)
                    .foregroundColor(Color.primaryVariant)
                    .font(Font.system(size: 14))
                Spacer().frame(height: 24)
                HStack {
                    HStack {
                        Image(systemName: "location.circle.fill").foregroundColor(.white)
                        Text(breed.origin).lineLimit(1).foregroundColor(.white)
                    }
                    Spacer().frame(width: 8)
                    Text(breed.temperament.split(separator: ",").first?.description ?? "")
                        .padding(.horizontal, 8)
                        .padding(.vertical, 4)
                        .foregroundColor(Color.primary)
                        .background(Color.primaryVariant)
                        .lineLimit(1)
                        .cornerRadius(.infinity)
                }
            }
            .padding(.horizontal, 16)
            .padding(.vertical, 8)
        }
    }
}
