//
//  BreedsListScreen.swift
//  TheCatAppIos
//
//  Created by Estiven on 28/07/23.
//

import Foundation
import SwiftUI

struct BreedsListScreen: View {
    
    @StateObject private var breedsViewModel = BreedsViewModel()
    @EnvironmentObject var networkMonitor: NetworkMonitor
    
    let columns = Array(repeating: GridItem(.flexible(), spacing: 20, alignment: .leading), count: 2)
    
    var body: some View {
        if networkMonitor.isConnected {
            NavigationView {
                ScrollView {
                    LazyVGrid(columns: columns, spacing: 20) {
                        ForEach(breedsViewModel.breedsList.indices,  id: \.self){ index in
                            let item = breedsViewModel.breedsList[index]
                            NavigationLink {
                                BreedDetailScreen(breed: item)
                            } label: {
                                ItemBreed(breed: item)
                                    .frame(maxWidth: .infinity)
                                    .background(Color("Purple40"))
                                    .cornerRadius(16)
                                    .onAppear {
                                        breedsViewModel.getImage(breed: item, breedList: breedsViewModel, index: index)
                                        
                                    }
                            }
                        }
                    }
                    .padding(.horizontal, 16)
                }
                .navigationTitle("TheCatApp")
            }
        } else {
            Text("no internet connection")
        }
    }
}

struct BreedsListScreen_Previews: PreviewProvider {
    static var previews: some View {
        BreedsListScreen()
    }
}
