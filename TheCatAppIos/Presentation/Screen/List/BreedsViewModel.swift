//
//  BreedsViewModel.swift
//  TheCatAppIos
//
//  Created by Estiven on 28/07/23.
//

import Foundation
import SwiftUI

class BreedsViewModel : ObservableObject {
    
    @Published var breedsList : [BreedsInDTO] = []
    
    
    init(breedsList: [BreedsInDTO] = [], urlImage: String = "") {
        self.breedsList = breedsList
        getAllBreeds()
    }
    
    func getAllBreeds() {
        NetworkManager.shared.getData(endpoint: "/v1/breeds") { (result: Result<[BreedsInDTO], NetworkError>) in
            
            switch result {
            case .success(let list):
                self.breedsList = list
            case .failure(let error):
                print("error \(error)")
            }
        }
    }
    
    func getImage(breed: BreedsInDTO, breedList: BreedsViewModel, index: Int){
        NetworkManager.shared.getData(
            endpoint: "/v1/images/\(breed.referenceImageId)"
        ) { (result: Result<ImagesInDTO, NetworkError>) in
            
            switch result {
            case .success(let imageInDTO):
                breedList.breedsList[index].image = imageInDTO.url
            case .failure(let error):
                print("error \(error)")
            }
        }
    }
}
