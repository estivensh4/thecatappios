//
//  BreedDetailFooter.swift
//  TheCatAppIos
//
//  Created by Estiven on 28/07/23.
//

import Foundation
import SwiftUI

struct BreedDetailFooter : View {
    
    @Environment(\.openURL) private var openURL
    var breed: BreedsInDTO
    
    var body: some View {
        
        let temperamentList = breed.temperament.split(separator: ",")
        
        VStack {
            HStack {
                Text(breed.name)
                    .bold()
                    .font(Font.system(size: 28))
                Spacer()
                HStack {
                    Text(breed.weight.metric)
                    Image(systemName: "dumbbell.fill")
                }
            }
            Spacer().frame(height: 12)
            HStack {
                HStack {
                    Image(systemName: "location.north.circle.fill")
                    Text(breed.origin)
                }
                Spacer()
                HStack {
                    Text(breed.lifeSpan)
                    Image(systemName: "pawprint.fill")
                }
            }
            ScrollView(.horizontal){
                LazyHStack {
                    ForEach(temperamentList.indices, id: \.self){ index in
                        Text(temperamentList[index])
                            .padding(.horizontal, 12)
                            .padding(.vertical, 6)
                            .foregroundColor(Color("Purple40"))
                            .background(Color.primaryVariant)
                            .cornerRadius(.infinity)
                        
                    }
                }
            }.frame(maxHeight: 60)
            Text("Summary")
                .frame(maxWidth: .infinity, alignment: .leading)
                .bold()
            Spacer().frame(height: 4)
            Text(breed.description).font(.system(size: 14))
            Spacer().frame(height: 24)
            Button {
                openURL(URL(string: breed.wikipediaUrl)!)
            } label: {
                Text("More info")
                    .frame(maxWidth: .infinity)
            }
            .buttonStyle(.borderedProminent).frame(maxWidth: .infinity)
            .tint(.primary)
        }
        .padding(.horizontal, 24)
        .padding(.vertical, 16)
        .background(.white)
        .cornerRadius(24, corners: [.topLeft, .topRight])
        .frame(maxHeight: .infinity, alignment: .bottom)
    }
}
