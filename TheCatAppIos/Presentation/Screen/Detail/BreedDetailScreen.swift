//
//  BreedDetailScreen.swift
//  TheCatAppIos
//
//  Created by Estiven on 28/07/23.
//

import Foundation
import SwiftUI

struct BreedDetailScreen: View {
    @Environment(\.presentationMode) var presentationMode
    @EnvironmentObject var networkMonitor: NetworkMonitor
    var breed: BreedsInDTO
    
    var body: some View {
        
        if networkMonitor.isConnected {
            
            NavigationView {
                ZStack(alignment: .top) {
                    
                    AsyncImage(url: URL(string: breed.image)) { image in
                        image.resizable()
                    } placeholder: {
                        ProgressView()
                            .progressViewStyle(CircularProgressViewStyle(tint: .white))
                            .scaleEffect(1.5, anchor: .center)
                    }
                    .frame(maxWidth: .infinity, maxHeight: 430)
                    
                    BreedDetailFooter(breed: breed)
                }
            }
        } else {
            Text("no internet connection")
        }
    }
}
